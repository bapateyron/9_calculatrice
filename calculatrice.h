#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define DEBUG
#ifdef DEBUG
   #define DUMPV(X) printf(#X"\n")
   #define DUMPI(X) printf(#X ": %d\n", X)
   #define DUMPC(X) printf(#X ": %c\n", X)
   #define DUMPS(X) printf(#X ": [%s]\n", X)
   #define DUMPD(X) printf(#X ": %lf\n", X)
#else
   #define DUMPV(X)
   #define DUMPI(X)
   #define DUMPC(X)
   #define DUMPS(X)
   #define DUMPD(X)
#endif

#define ALLOUER(PTR, TYPE, NOMBRE)\
   if((PTR = (TYPE*)malloc(sizeof(TYPE)*NOMBRE))\
	 == NULL)\
	 puts("Echec d'allocation")

typedef enum ope {
   ID,	 // 0
   SIN,
   COS,
   LOG,
   EXP,	 // 4
   NONE	 // 5

} OP;

OP identification(char * expression);
double	 evaln(double x, OP o);
double	 evalp(double x, OP o);
void	 calcul(double a, double b, double delta, OP o);
void	 saisie(char * buffer, int size, FILE * stream);
void	 split2(char * string, OP * o, double * val);
double	 identite(double x);
double	 erreur(double x);

