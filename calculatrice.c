#include "calculatrice.h"

const char * OPER_NAMES[] = { "x", "sin(x)", "cos(x)", "log(x)", "exp(x)", NULL};

// MAIN //
int main()
{
   puts("Hello world");

   // printf("Eval sin(0): %lf\n", evaln(0, COS));
   
   char	    buffer[20]  = {0};
   OP	    op		= NONE;
   double   val		= 0;

   saisie(buffer, 20, stdin);
   split2(buffer, &op, &val);

   // printf("V: %lf\n", val);
   
   calcul(0, val, 1, op);

   return 0;
}

/*******************************************************************************
 * @parm expresison: string  laquelle on cherche une fonction mathematique
 * @return: un OPerateur ou 'NONE' si rien trouve
 *
 ******************************************************************************/
OP identification(char * expression)
{
   int	 i	     = 0;
   int	 estPresent  = 0;
   OP	 o	     = NONE;	   // Enumeration
   while(OPER_NAMES[i] && !estPresent)
   {
      if(!strcmp(OPER_NAMES[i], expression)) estPresent = 1;
      else				     i++;
      // printf("%s\n", OPER_NAMES[i]);
   }

   if(!OPER_NAMES[i])	o = NONE;
   else			o = (OP) i;

   return o;
}

// Evaln
double evaln(double x, OP o)
{
   double r = 0;

   switch(o)
   {
      case ID:	  r = x;      break;
      case SIN:	  r = sin(x); break;
      case COS:	  r = cos(x); break;
      case LOG:	  r = log(x); break;
      case EXP:	  r = exp(x); break;
      case NONE:  puts("evaln(): Fonction inconnue"); break;
      default:	  puts("evaln(): Echec de l'evaluation");
   }

   return r;
}


// Pointeur de fonction
double	 evalp(double x, OP o)
{
   double (*OPER_FN[])(double) = { &identite, &sin, &cos, &log, &exp, &erreur };

   return OPER_FN[o](x);
}

// Calcul
void calcul(double a, double b, double delta, OP o)
{
   for(double i = a; i <= b; i += delta)
   {
      printf("calcul(%lf): %lf\n", i, evalp(i, o));
   }
}

// Saisie
void saisie(char * buffer, int size, FILE * stream)
{
   int i = 0;

   printf("Saisissez un valeur avec le format suivant:\nCOS 15\n-- ou alors --\nCOS\n15\n\n> ");

   fgets(buffer, size, stream);

   while(buffer[i] != '\n') i++;
   buffer[i] = 0;
}

// Split2
void split2(char * str, OP * o, double * val)
{
   int	 i	     = 0;
   char	 buffer[4]   = {0};

   while(str[i] != ' ' && str[i] != '\n')
   {
      buffer[i] = toupper(str[i]);
      i++;
   }
   
   if	    (!strcmp(buffer, "ID"))     * o = ID;
   else if  (!strcmp(buffer, "SIN"))    * o = SIN;
   else if  (!strcmp(buffer, "COS"))    * o = COS;
   else if  (!strcmp(buffer, "LOG"))    * o = LOG;
   else if  (!strcmp(buffer, "EXP"))    * o = EXP;
   else if  (!strcmp(buffer, "NONE"))   * o = NONE;
   else				       puts("split2()): Echec de l'evaluation");

   *val = atof(&str[i+1]);
}

// Identite
double	 identite(double x)
{
   return x;
}

// Erreur
double	 erreur(double x)
{
   DUMPD(x);
   fprintf(stderr, "erreur(): Fonction inconnue\n");
   return x;
}
