FLAGS :=  -g -Wall -Wextra
LIB := -lm
LIB1 := -lX11
LIB2 := -lSDL2 -lSDL2_ttf

all: calculatrice.bin

calculatrice.bin: calculatrice.o
	gcc -o calc calculatrice.c $(FLAGS) $(LIB)

calculatrice.o: calculatrice.c
	gcc -c calculatrice.c 

clean:
	rm -f *.o *~
